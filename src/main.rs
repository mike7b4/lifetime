use std::collections::HashMap;

#[derive(Debug)]
struct Foo {
    id: String,
}

impl Foo {
    pub fn new(foo: &str) -> Self {
        Self {
            id: String::from(foo)
        }
    }
}

#[derive(Debug)]
struct Bar<'a> {
    foos: HashMap<&'a str, &'a Foo>,
    bar: String,
}

impl <'a>Bar<'a> {
    fn new() -> Self {
        Self {
            foos: HashMap::new(),
            bar: String::from("bar")
        }
    }
}

fn main() {
    let mut vecs: Vec<Foo> = Vec::new();
    vecs.push(Foo::new("foo2"));
    vecs.push(Foo::new("foo3"));
    vecs.push(Foo::new("foo1"));
    vecs.sort_by(|a, b| a.id.cmp(&b.id));
    println!("{:?}", vecs);
    let mut bar = Bar::new();
    for foo in &vecs {
        bar.foos.insert(&foo.id, &foo);
    }

    println!("Hello, world! {:?} {}", bar.foos, bar.bar);
}
